# Projeto: Desafio 2 do módulo Docker  do Curso FullCycle 3.0 
## Curso: FullCycle 3.0
## Módulo: Docker  
## Desafio 2  
  
Repositório do projeto no GitLab: https://gitlab.com/fdmneto/fullcycle_docker_d2   


### Para instalar e executar:  
1. Clonar o projeto do GitLab em https://gitlab.com/fdmneto/fullcycle_docker_d2.git
3. Na pasta raiz do projeto executar o comando "docker compose up -d".  
OBS: Os dados do banco de dados ficarão no diretório mysql/data/.  
4. Usar um browser acessando o endereço localhost:8080

### Ao ser executado o projeto criará no docker:  
* A network fullcycle_docker_d2_nw;  
* As imagens:  
fdmneto/fullcycle_docker_d2_db  
fdmneto/fullcycle_docker_d2_app  
fdmneto/fullcycle_docker_d2_nginx  
* Os containers:  
db   - contém o banco de dados mysql  
app  - contém a aplicação Node  
nginx  - contém o proxy reverso  

### Estrutura do projeto:  
1. Arquivos Dockerfile: Os dockerfiles foram feitos de tal modo que a montagem dos dockers pudesse ser tanto com o compose quanto individualmente mantendo o conjunto final funcionando.
1.1 Dockerfile.mysql: 
Usa como base a imagem mysql:5.7;
Cria as variáveis de ambiente necessárias à criação do banco de dados nodedb;  
Adiciona o arquivo com o script SQL que criará a tabela people e o primeiro registro com um nome cadastrado.  
1.2 Dockerfile.node:  
Usa como base a imagem node:16;
Copia os arquivos do projeto em Node;  
Instala as dependências do Node  
Instala o aplicativo Dockerize  
1.3 Dockerfile.nginx:  
Usa como base a imagem nginx:1.15.0;  
Substitue o arquivo default.conf pelo arquivo nginx.conf.  
2. Diretórios:  
2.1 mysql:  
Contém o script sql init_database.sql responsável por iniciar a base de dados com a tabela people e o primeiro registro;  
2.2 nginx:  
Contém o arquivo nginx.conf com as configurações necessárias para fazer o proxy reverso e também fazer com que o container continue rodando mesmo que o container "app" ainda não tenha iniciado.  
2.3 node: Contém os arquivos do projeto Node.  

  
&nbsp;  
&nbsp;  

#### Estudo de como fazer o desafio 2 sem o uso do docker compose
1. Executar os comandos:  
docker network create --driver bridge fullcycle_docker_d2_nw  

2. No diretório fullcycle_docker_d2 executar os comandos:  
sudo docker build -t fdmneto/fullcycle_docker_d2_db -f ./Dockerfile.mysql .
docker run -d --name db --network fullcycle_docker_d2_nw --network-alias=db \
--env="MYSQL_ROOT_PASSWORD=root" \
--env="MYSQL_DATABASE=nodedb" \
-v ./mysql/data:/var/lib/mysql \
fdmneto/fullcycle_docker_d2_db  

3. No diretório node o comando:  
npm install  

4. No diretório fullcycle_docker_d2 executar os comandos:  
sudo docker build -t fdmneto/fullcycle_docker_d2_app -f ./Dockerfile.node .  
docker run -d --name app --network fullcycle_docker_d2_nw --network-alias=app -v ./node:/usr/src/app fdmneto/fullcycle_docker_d2_app  

5. No diretório ./nginx executar os comandos:  
sudo docker build -t fdmneto/fullcycle_docker_d2_nginx -f ./Dockerfile.nginx .  
docker run -d --name nginx --network fullcycle_docker_d2_nw --network-alias=nginx -p 8080:80 fdmneto/fullcycle_docker_d2_nginx  



#### Configuração para teste do app e da base de dados sem o nginx para debug:  
sudo docker build -t fdmneto/fullcycle_docker_d2_app -f ./Dockerfile.node .
docker run -d --name app --network fullcycle_docker_d2_app -p 3000:3000 fdmneto/fullcycle_docker_d2_app

sudo docker build -t fdmneto/fullcycle_docker_d2_db -f ./Dockerfile.mysql .
docker run -d --name db -p 3306:3306 \
--env="MYSQL_ROOT_PASSWORD=root" \
--env="MYSQL_DATABASE=nodedb" \
-v ./mysql/data:/var/lib/mysql \
fdmneto/fullcycle_docker_d2_db