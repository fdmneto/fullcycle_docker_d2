import express from "express";
import bodyParser from "body-parser";
import mysql from "mysql"

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));

const port = 3000;
const config = {
    host: 'db',
    user: 'root',
    password: 'root',
    database: 'nodedb'
}
const db = mysql.createConnection(config);

let sql = '';

app.get("/",(req,res) => {
    lerListaNomes(db)
    .then(function(lista){
        res.send(gerarPagina(lista));
    })
});

app.post("/",(req,res) => {
    inserirNome(db,req.body.nome)
    .then(function(){
        res.redirect("/");       
    });
});

app.listen(port,()=>{
    console.log(`Servidor escutando na porta: ${port}`);
})


function gerarPagina(lista){
    let paginaStr = `<!DOCTYPE html><html><body>
    <div><h1>Full Cycle Rocks!<h1></div>
    <div><p><strong>Lista de Nomes:</strong>${lista}</p></div>
    <div>
        <form action="/" method="post">
            <text><strong>Inserir novo nome</strong><br></text>
            <label for="nome">Novo Nome:</label>
            <input type="text" id="nome" name="nome">
            <input type="submit" value="Submit">
        </form>
    </div>
    </body></html>`;
    return paginaStr;
}

function lerListaNomes(db){
    return new Promise((resolve, reject) => {
        let listaNomes = '';
        sql = 'select nome from people';
        db.query(sql, function (err, result) {
            if (err) {
                console.log("erro na consulta: err ==========================>",err);
                reject(err);
            } 
            if(result){
                result.forEach(function(row) {
                    listaNomes = listaNomes + '<br>' + row.nome; 
                });
            }
            else{
                listaNomes = "Erro de consulta no banco de dados";
            }
            resolve(listaNomes);
        });
    });
}

async function inserirNome(db, nome){
    let listaNomes = '';
    
    if(nome) {
        sql = `insert into people (nome) VALUES ("${nome}")`;
        db.query(sql, function (err, result) {
            if (err) throw err;
        });
    }
}


