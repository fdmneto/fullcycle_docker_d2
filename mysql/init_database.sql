USE nodedb;
CREATE TABLE people (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(250) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY people_id_IDX (id) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO people(nome) VALUES ('Primeiro da Lista');